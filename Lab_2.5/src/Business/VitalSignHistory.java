/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author Zang
 */
public class VitalSignHistory {
    private ArrayList<VitalSigns> vitalSignHistory;
    //private List<VitalSigns> vitalSignList;

    //Constructor
    public VitalSignHistory(){
        this.vitalSignHistory = new ArrayList<VitalSigns>();
        //this.vitalSignList = new ArrayList<VitalSigns>();
    }

    public ArrayList<VitalSigns> getVitalSignHistory() {
        return vitalSignHistory;
    }
    
    public ArrayList<VitalSigns> getAbnormalList(double maxbp, double minbp){
        List<VitalSigns> abnList = new ArrayList<>();
        for(VitalSigns vs:vitalSignHistory){
            if(vs.getBloodPressure() > maxbp || vs.getBloodPressure() < minbp){
                System.out.println(vs.getBloodPressure());
                abnList.add(vs);
            }
        }
        return (ArrayList<VitalSigns>) abnList;
    }

    public void setVitalSignHistory(ArrayList<VitalSigns> vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    
    public VitalSigns addVitals(){
        VitalSigns vs = new VitalSigns();
        this.vitalSignHistory.add(vs);
        return vs;
    }
    
    public void deleteVitals(VitalSigns v){
        vitalSignHistory.remove(v);
    }
    
}
