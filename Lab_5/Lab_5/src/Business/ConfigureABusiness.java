/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.UserAccount.UserAccount;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();
        
        //create admin organization
        AdminOrganization adminOrganization = new AdminOrganization();
        //add created admin organization into Business
        business.getOrganizationDirectory().getOrganizationList().add(adminOrganization);
        
        //create doctor organization
        DoctorOrganization doctorOrganization = new DoctorOrganization();
        //add created doctor organization into business
        business.getOrganizationDirectory().getOrganizationList().add(doctorOrganization);
        
        //create lab assistant organization
        LabOrganization labOrganization = new LabOrganization();
        //add created lab into business
        business.getOrganizationDirectory().getOrganizationList().add(labOrganization);
        
        
        //create a new employee for admin
        Employee employee = new Employee();
        employee.setName(" Sam Smith");
        
        //create a new employee1 for doctor
        Employee employee1 = new Employee();
        employee1.setName(" Jackie");
        
        //create a new employee2 for lab assistant
        Employee employee2 = new Employee();
        employee2.setName(" Ben");
        
        
        //create a new account for admin
        UserAccount account = new UserAccount();
        account.setUsername("admin");
        account.setPassword("admin");
        account.setRole("Admin");
        //assign the account to employee
        account.setEmployee(employee);
        
        //create a new account_1 for doctor
        UserAccount account_1 = new UserAccount();
        account_1.setUsername("doctor");
        account_1.setPassword("doctor");
        account_1.setRole("Doctor");
        //assign the account_1 to employee1
        account_1.setEmployee(employee1);
        
        //create a new account_2 for lab assistant
        UserAccount account_2 = new UserAccount();
        account_2.setUsername("lab");
        account_2.setPassword("lab");
        account_2.setRole("Lab Assistant");
        //assign the account_2 to employee2
        account_2.setEmployee(employee2);
        
        
        //add the employee into admin organization
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee);
        //add the account into admin organization
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account);
        
        //add the employee1 into doctor organization
        doctorOrganization.getEmployeeDirectory().getEmployeeList().add(employee1);
        //add the account_1 into doctor organization
        doctorOrganization.getUserAccountDirectory().getUserAccountList().add(account_1);
        
        //add the employee2 into lab organization
        labOrganization.getEmployeeDirectory().getEmployeeList().add(employee2);
        //add the account_2 into lab organization
        labOrganization.getUserAccountDirectory().getUserAccountList().add(account_2);
        return business;
    }
    
}
