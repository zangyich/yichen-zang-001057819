/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;
import Business.ProductDirectory;

/**
 *
 * @author AEDSpring2019
 */
public class Customer extends User implements Comparable<Customer>{
    //private ProductDirectory directory;
    private Date date;

    public Customer(String password, String userName, String role) {
        super(password, userName, role);
        //directory = new ProductDirectory();
        date = new Date();
        //Date date = new Date();
        //System.out.println(date);
        //this.directory = directory;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

//    public ProductDirectory getDirectory() {
//        return directory;
//    }
//
//    public void setDirectory(ProductDirectory directory) {
//        this.directory = directory;
//    }
    
    //@Override
    public int compareTo(Supplier o) {
        return o.getUserName().compareTo(this.getUserName());
    }
    
    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
//    @Override
//    public String toStringdate(){
//        return getDate();
//    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }

    @Override
    public int compareTo(Customer o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
