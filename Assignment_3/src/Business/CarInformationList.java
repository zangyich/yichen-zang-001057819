/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 *
 * @author Zang
 */
public class CarInformationList {
    private static CarInformationList carLists;
    public List<Car> carList;
    public List<Car> carlistbrandresult;
    public List<Car> carlistyear;
    public List<Car> carcity;
    public List<Car> carfirstavail;
    public List<Car> carabnormal;
    public List<Car> carlast;
    public List<Car> carmodel;
    public List<Car> carmanu;
    public List<Car> carcertificate;
    public List<Car> caravail;
    public List<Car> caravail2;
    
    public CarInformationList(){
        carList = new ArrayList<>();
        
        Car car1 = new Car(true, "Ferrari", 2019, 1, 2, 1,"Red One", "Boston", true);
        Car car2 = new Car(true, "BMW", 2019, 1, 4, 2, "X1", "New York", true);
        Car car3 = new Car(true, "Toyota", 2018, 1, 4, 3, "T1", "Boston", true);
        Car car4 = new Car(true, "GM", 2018, 1, 4, 4, "G1", "New York", true);
        Car car5 = new Car(true, "Toyota", 2017, 1, 4, 5, "T2", "Chicago", true);
        Car car6 = new Car(true, "GM", 2017, 1, 4, 6, "G2", "Chicago", true);
        Car car7 = new Car(true, "Ferrari", 2016, 1, 4, 7, "", "Seattle", true);
        Car car8 = new Car(true, "BMW", 2016, 1, 4, 8, "X2", "Seattle", true);
        Car car9 = new Car(true, "Toyota", 2019, 1, 4, 9, "T3", "Austin", true);
        Car car10 = new Car(true, "GM", 2019, 1, 4, 10, "G3", "Austin", true);
        
        carList.add(car1);
        carList.add(car2);
        carList.add(car3);
        carList.add(car4);
        carList.add(car5);
        carList.add(car6);
        carList.add(car7);
        carList.add(car8);
        carList.add(car9);
        carList.add(car10);
    }
   
    
    public static CarInformationList  getCarList() {
        carLists = new CarInformationList();
        return carLists;
    }
    
    public List<Car> getCarInfoList(){
        return carList;
    }
    
    public void setCarList(List<Car> carList){
        this.carList = carList;
    }
    
    public Car addCar(){
        Car car = new Car();
        carList.add(car);
        return car;
    }
    
    public void deleteCar(Car car){
        carList.remove(car);
    }
    
    public List<Car> searchCarBrand(String brand){
        carlistbrandresult = new ArrayList<>();
        for(Car car: this.carList){
            if(car.getBrand().equalsIgnoreCase(brand)){
                //System.out.println(brand);
                carlistbrandresult.add(car);
                
            }
        }
        return carlistbrandresult;
    }
    
    public List<Car> getbrandresult(){
        return carlistbrandresult;
    }
    
    public List<Car> searchYear(int manufactured_year){
        carlistyear = new ArrayList<>();
        for(Car car : this.carList){
            if(car.getManufactured_year() == manufactured_year){
                carlistyear.add(car);
            }
        }
        return carlistyear;
    }
    
    public List<Car> getyearresult(){
        return carlistyear;
    }
    
    public Car searchSerialNum(int serial_num){
        for(Car car : this.carList){
            if(car.getSerial_num() == serial_num){
                //System.out.println(serial_num);
                return car;
            }
        }
        return null;
    }
    
    public List<Car> searchCity(String availble_city){
        carcity = new ArrayList<>();
        for (Car car : this.carList){
            if(car.getAvailble_city().equalsIgnoreCase(availble_city)){
                carcity.add(car);
            }
        }
        return carcity;
    }
    
    public List<Car> getcityresult(){
        return carcity;
    }
    
    public List<Car> searchFirst(boolean available){
        carfirstavail = new ArrayList<>();
        for (Car car : this.carList){
            if(car.isAvailable() == available){
                carfirstavail.add(car);
                return carfirstavail;
            } 
        }
        return null;
    }
    
    public List<Car> getfirstresult(){
        return carfirstavail;
    }
    
    public List<Car> searchAbnormalList(double maxSeat, double minSeat){
        carabnormal = new ArrayList<>();
        for(Car car : this.carList){
            if(car.getMax_seats() < maxSeat || car.getMin_seats() > minSeat){
                //System.out.println(maxSeat);
                carabnormal.add(car);
            }
        }
        return carabnormal;
    }
//    public Car getyear(int manufactured_year, List<Car> carList){
//        for(Car car : this.carList){
//            if(car.getManufactured_year()==manufactured_year){
//                return car;
//            }
//        }
//        return null;
//    }
    
    public List<Car> searchLast(){
        carlast = new ArrayList<>();
        for (Car car : this.carList){
            if(car.getManufactured_year() == 2019){
                carlast.add(car);
                System.out.println(carlast);
                return carlast;
            } 
        }
        return null;
    }
    
    public List<Car> getsearchlast(){
        return carlast;
    }
    
    public boolean contain(Car car) {
        return carList.contains(car);
    }
    
    public List<Car> searchModel(){
        carmodel = new ArrayList<>();
        for (Car car : this.carList){
            if(car.getModel_num().contains("X1")){
                carmodel.add(car);
                //System.out.println(false);
            }
            if(car.getModel_num().contains("X2")){
                carmodel.add(car);
                //System.out.println(true);
            }
            if(car.getModel_num().contains("G1")){
                carmodel.add(car);
            }
            if(car.getModel_num().contains("G2")){
                carmodel.add(car);
            }
            if(car.getModel_num().contains("G3")){
                carmodel.add(car);
            }
            if(car.getModel_num().contains("T1")){
                carmodel.add(car);
            }
            if(car.getModel_num().contains("T2")){
                carmodel.add(car);
            }
            if(car.getModel_num().contains("T3")){
                carmodel.add(car);
            }
            if(car.getModel_num().contains("Red One")){
                carmodel.add(car);
            }
        }
        return carmodel;
    }

    public List<Car> getsearchmodel(){
        return carmodel;
    }

    public List<Car> searchManufactuer(){
        carmanu = new ArrayList<>();
        for (Car car : this.carList){
            //String care = car.getBrand();
            if(car.getBrand().equalsIgnoreCase("")){
                //carmanu.add(car);
                //return null;
            }
            else{
                carmanu.add(car);
            }
        } 
        return carmanu;
    }
    
    public List<Car> getmanufacturer(){
        return carmanu;
    }
    
    public List<Car> searchCertificate(){
        carcertificate = new ArrayList<>();
        for (Car car : this.carList){
            //String care = car.getBrand();
            if(car.isMaintenance_certificate()){
                //carcertificate.add(car);
                //return null;
            }
            else{
                carcertificate.add(car);
            }
            
        } 
        return carcertificate;
    }
    
    public List<Car> getcertificate(){
        return carcertificate;
    }
    
    public int searchAvailCar(){
        caravail = new ArrayList<>();
        for (Car car : this.carList){
            if(car.isAvailable()){
                caravail.add(car);
            }            
        } 
        int c = caravail.size();
        return c;
    }

    public int searchNonAvailCar(){
        caravail2 = new ArrayList<>();
        for (Car car : this.carList){
            if(car.isAvailable()){
                //caravail.add(car);
            }
            else{
                caravail2.add(car);
            }
        } 
        int c = caravail2.size();
        return c;
    }
}
